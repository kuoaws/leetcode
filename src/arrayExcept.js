// except(差集)集合运算：
// 先将其中完全重复的数据行删除，
// 再返回只在第一个集合中出现，在第二个集合中不出现的所有行

const arrayExcept = (array1 = [], array2 = []) => {
    return array1.filter(x => !array2.includes(x))
}

console.log(arrayExcept([1, 2, 3, 4, 5], [2, 4, 6, 8, 10]))