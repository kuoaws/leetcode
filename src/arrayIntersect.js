const arrayIntersect = (array1 = [], array2 = []) => {
    return array1.filter(x => array2.includes(x))
}

console.log(arrayIntersect([1, 2, 3, 4, 5], [2, 4, 6, 8, 10]))