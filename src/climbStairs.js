/**
 * @param {number} n
 * @return {number}
 */


// var climbStairs = function(n) {
//     if (n <= 2) {
//         return n;
//     }

//     return climbStairs(n - 1) + climbStairs(n - 2)
// };


const climbStairs = (n) => {
    if (n <= 2) {
        return n
    }

    let result = 0;
    let pre = 1;
    let next = 2;

    for (let i = 2; i < n; i++) {
        result = pre + next;
        pre = next;
        next = result
    }

    return result;
}

console.log(climbStairs(2))