const fib = (n) => {
    if (n < 2) {
        return n
    }

    const temp = fib(n - 1) + fib(n - 2);
    console.log(temp)

    return temp
}

console.log(fib(10))