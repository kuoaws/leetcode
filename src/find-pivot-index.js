const findPivotIndex = (nums = []) => {

    let leftSum = 0;

    let sum = nums.reduce((acc, cur) => acc + cur);

    for (let i = 0; i < nums.length; i++) {
        if (2 * leftSum + nums[i] == sum) {
            return i;
        }

        leftSum += nums[i];
    }

    return -1;
}

console.log(findPivotIndex([2, 1, -1]))