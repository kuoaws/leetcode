const longestCommonPrefix = (array = []) => {

    if (array.length === 0) {
        return '';
    }

    let prefix = array[0];

    for (let i = 1; i < array.length; i++) {

        let sliceIndex = -1;
        let str = array[i];

        for (let j = 0; j < prefix.length; j++) {
            if (prefix[j] !== str[j]) {
                break;
            } else {
                sliceIndex = j;
            }
        }

        prefix = sliceIndex > -1 ? prefix.slice(0, sliceIndex + 1) : ''
    }

    return prefix;
}

console.log(longestCommonPrefix(["flower", "flow", "flight"]))
