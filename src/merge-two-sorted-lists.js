class ListNode {
    constructor(val = 0) {
        this.val = val;
        this.next = null;
    }
}

const mergeTwoLists = (l1, l2) => {
    let result = new ListNode(0);
    let c = result;

    while (l1 !== null && l2 != null) {
        if (l1.val < l2.val) {
            c.next = l1;
            l1 = l1.next;
        } else {
            c.next = l2;
            l2 = l2.next
        }

        c = c.next
    }

    if (l1 !== null) {
        c.next = l1;
    }

    if (l2 !== null) {
        c.next = l2;
    }

    return result.next;
}

