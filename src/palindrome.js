const palindrome = (number = 0) => {

    const reverse = number
        .toString()
        .split('')
        .reverse()
        .join('')

    return number.toString() === reverse

}

console.log(palindrome(12321))