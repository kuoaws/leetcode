/**
 * @param {number[]} nums
 * @return {number}
 */

const removeDuplicates = (nums) => {
    let i = 0;
    for (let j = i + 1; j < nums.length; j++) {
        if (nums[i] !== nums[j]) {
            i++;
            nums[i] = nums[j];
        }
    }

    const numSize = i + 1;
    return nums.slice(0, numSize)
}

console.log(removeDuplicates([1, 1, 2, 2, 3, 3, 3, 4]))