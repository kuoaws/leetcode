const romanToInt = (str) => {
    const map = new Map();
    map.set("I", 1);
    map.set("V", 5);
    map.set("X", 10);
    map.set("L", 50);
    map.set("C", 100);
    map.set("D", 500);
    map.set("M", 1000);

    let result = 0;

    for (let i = 0; i < str.length; i++) {
        let v1 = map.get(str[i]);
        let v2 = map.get(str[i + 1]);
        if (v1 < v2) {
            result += v2 - v1;
            i++;
        } else {
            result += v1;
        }
    }

    return result;
}

console.log(romanToInt("MCMXCIV"))