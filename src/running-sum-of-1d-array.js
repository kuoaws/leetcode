const runningSum = (nums = []) => {

    let temp = 0;

    let result = nums.map(x => {
        temp += x;
        return temp;
    })

    return result;
}

console.log(runningSum([3, 1, 2, 10, 1]))