const twoSum = (ary = [], target) => {

    // ## solution 1
    // for (let i = 0; i < ary.length; i++) {
    //     const first = ary[i];

    //     for (let j = i + 1; j < ary.length; j++) {
    //         const second = ary[j];

    //         if (first + second == target) {
    //             return [i, j]
    //         }
    //     }
    // }

    // ## solution 2
    // let waitPairs = []
    // for (let x of ary) {
    //     let y = target - x;
    //     if (waitPairs.indexOf(y) > -1) {
    //         const indexX = ary.indexOf(x);
    //         const indexY = ary.indexOf(y);

    //         return [indexX, indexY]

    //     } else {
    //         waitPairs.push(x);
    //     }
    // }

    // ## solution 3
    let storage = {};
    for (let [index, num] of ary.entries()) {
        if (storage[num] !== undefined) {
            return [storage[num], index]
        }

        storage[target - num] = index
    }
}

console.log(twoSum([1, 6, 4, 5, 3, 2], 8));
