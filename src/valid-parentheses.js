const isValid = (s = '') => {

    var left = ['(', '{', '['];
    var right = [')', '}', ']'];

    const map = new Map();
    map.set('(', ')');
    map.set('{', '}');
    map.set('[', ']');

    var ary = s.split('');

    var stack = [];

    for (let c of ary) {
        if (left.indexOf(c) !== -1) {
            stack.push(c);
        }
        else if (right.indexOf(c) !== -1) {
            var top = stack[stack.length - 1];
            if (map.get(top) === c) {
                stack.pop();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    return stack.length === 0;
}

console.log(isValid("()[]{}"))